package com.example.android.datafrominternet;

/**
 * Created by vishalamidala on 20/11/17.
 */

public class Listitem {

    private String id;
    private String name;

   public Listitem(String id, String name){
       this.id = id;
       this.name = name;
   }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}

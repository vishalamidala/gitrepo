package com.example.android.datafrominternet;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;

/**
 * Created by vishalamidala on 20/11/17.
 */

public class SimpleAdapter extends RecyclerView.Adapter<SimpleAdapter.SimpleAdapterViewHolder> {


    private List<Listitem> listItems;
    private Activity activity;

    public SimpleAdapter(List<Listitem> listItems, Activity activity) {
        this.listItems = listItems;
        this.activity = activity;
    }

    @Override
    public SimpleAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);
        return new SimpleAdapterViewHolder(v);
    }

    @Override
    public void onBindViewHolder(SimpleAdapterViewHolder holder, int position) {

    Listitem listItem = listItems.get(position);
    holder.tv_id.setText(listItem.getId());
    holder.tv_name.setText(listItem.getName());


    }

    @Override
    public int getItemCount() {
        return listItems.size();


    }

    public class SimpleAdapterViewHolder extends RecyclerView.ViewHolder{

        public TextView tv_id;
        public TextView tv_name;

        public SimpleAdapterViewHolder(View itemView) {
            super(itemView);
            tv_id = (TextView) itemView.findViewById(R.id.tv_id);
            tv_name = (TextView) itemView.findViewById(R.id.tv_name);

        }
    }
}


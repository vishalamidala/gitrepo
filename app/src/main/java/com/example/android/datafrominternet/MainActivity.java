

package com.example.android.datafrominternet;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;


import android.widget.SimpleAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import com.example.android.datafrominternet.utilities.NetworkUtils;

import java.util.List;

import java.io.IOException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    private EditText mSearchBoxEditText;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView mListView;

    ArrayList<HashMap<String, String>> githubSearchResultList;

    private List<Listitem> listItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        githubSearchResultList = new ArrayList<>();

        mSearchBoxEditText = (EditText) findViewById(R.id.et_search_box);

        mListView = (RecyclerView) findViewById(R.id.list_view);
        mListView.setHasFixedSize(true);
        mListView.setLayoutManager(new LinearLayoutManager(this));




     listItems = new ArrayList<>();
     mAdapter = new SimpleAdapter(listItems, this);
     mListView.setAdapter(mAdapter);

makeGithubSearchQuery();

    }

    /**
     * This method retrieves the search text from the EditText, constructs the
     * URL (using {@link NetworkUtils}) for the github repository you'd like to find, displays
     * that URL in a TextView, and finally fires off an AsyncTask to perform the GET request using
     * our {@link GithubQueryTask}
     */
    private void makeGithubSearchQuery() {
        String githubQuery = mSearchBoxEditText.getText().toString();
        URL githubSearchUrl = NetworkUtils.buildUrl(githubQuery);

        // COMPLETED (4) Create a new GithubQueryTask and call its execute method, passing in the url to query
        new GithubQueryTask().execute(githubSearchUrl);
    }

    // COMPLETED (1) Create a class called GithubQueryTask that extends AsyncTask<URL, Void, String>
    public class GithubQueryTask extends AsyncTask<URL, Context, String> {


        // COMPLETED (2) Override the doInBackground method to perform the query. Return the results. (Hint: You've already written the code to perform the query)
        @Override
        protected String doInBackground(URL... params) {
            URL searchUrl = params[0];
            String githubSearchResults = null;

            try {
                githubSearchResults = NetworkUtils.getResponseFromHttpUrl(searchUrl);

                JSONObject jsonObj = new JSONObject(githubSearchResults);


                JSONArray items = jsonObj.getJSONArray("items");


                for (int i = 0; i < items.length(); i++) {
                    JSONObject c = items.getJSONObject(i);

                    Listitem item = new Listitem(
                            c.getString("id"),
                            c.getString("name")
                    );
                    listItems.add(item);
                }



            }catch(JSONException e){
                e.printStackTrace();
            }

            catch (IOException e) {
                e.printStackTrace();
            }


            return null;
    }

        @Override
        protected void onPostExecute(String s) {
            mAdapter.notifyDataSetChanged();

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemThatWasClickedId = item.getItemId();
        if (itemThatWasClickedId == R.id.action_search) {
            makeGithubSearchQuery();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
